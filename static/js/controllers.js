'use strict';

/* Controllers */

function ChartCtrl($scope, wsSrv) {
    console.log("hello world")
    $scope.error = {msg:""}
    $scope.messages = [];

    $scope.counter = 3;
    $scope.chart = {}
    $scope.chart.dataX = [[]];
    $scope.chart.dataY = [[]];
    $scope.chart.dataZ = [[]];

    wsSrv.messageHandler = function(msg){
        var obj = JSON.parse(msg);
        $scope.chart.dataX[0].push([$scope.counter++,obj.FloatXValue])
        if($scope.chart.dataX[0].length > 100){
            $scope.chart.dataX[0].shift();
        }
        $scope.chart.dataY[0].push([$scope.counter++,obj.FloatYValue])
        if($scope.chart.dataY[0].length > 100){
            $scope.chart.dataY[0].shift();
        }
        $scope.chart.dataZ[0].push([$scope.counter++,obj.FloatZValue])
        if($scope.chart.dataZ[0].length > 100){
            $scope.chart.dataZ[0].shift();
        }
    }

}
ChartCtrl.$inject = ['$scope', 'webSocketFactory'];
