package main

import (
	"code.google.com/p/go.net/websocket"
	"fmt"
	"io"
	"log"
	"math/rand"
	"net"
	"net/http"
	"os"
	"os/exec"
	"runtime"
	"strconv"
	"strings"
	"time"
	//"time"
)

var accelerometr chan string

func main() {
	accelerometr = make(chan string, 1000)
	go getFromAndroid()
	websocketpage()
}

func getFromAndroid() {
	log.Println("START 1")
	service := ":5005"
	tcpAddr, err := net.ResolveTCPAddr("tcp4", service)
	checkError(err)

	listener, err := net.ListenTCP("tcp", tcpAddr)
	checkError(err)
	for {
		conn, err := listener.Accept()
		if err != nil {
			//continue
		}
		log.Println("CONNECT ")
	loopfor:
		for {
			var buf [512]byte
			_, err := conn.Read(buf[0:])
			//log.Println("MSG ", string(buf[0:]))
			accelerometr <- string(buf[0:])

			if err == io.EOF {
				log.Println("END ")
				conn.Close()
				break loopfor
			}
			//<-time.After(70 * time.Millisecond)
		}
	}
}

func checkError(err error) {
	if err != nil {
		fmt.Fprintf(os.Stderr, "Fatal error: %s", err.Error())
		os.Exit(1)
	}
}

var port int = 23456
var tick time.Duration = 500 * time.Millisecond
var openBrowser bool = false
var verbose bool = false

type hub struct {
	connections map[*websocket.Conn]bool
	name        string
}

var hubs map[string]*hub

func (h hub) register(conn *websocket.Conn) {
	h.connections[conn] = true
}

func (h hub) unregister(conn *websocket.Conn) {
	delete(h.connections, conn)
}

func (h hub) broadcast(message string) {
	var err error
	for ws := range h.connections {
		err = websocket.Message.Send(ws, message)
		if err != nil {
			log.Println(err)
			break
		}
	}
}

func wsServer(ws *websocket.Conn) {
	log.Println("Connect browser")
	hub := hubs["time"] //TODO extract from ws config
	hub.register(ws)
	if verbose {
		log.Println("registered for ws")
	}

	for {
		var buf string
		err := websocket.Message.Receive(ws, &buf)
		if err != nil {
			log.Println(err)
			break
		}
		time.Sleep(100 * time.Millisecond)
	}
	hub.unregister(ws)
	if verbose {
		log.Println("unregistered for ws")
	}
}

type Event struct {
	TimeStamp   int64
	FloatXValue float64
	FloatYValue float64
	FloatZValue float64
	StringValue string
}

var r *rand.Rand = rand.New(rand.NewSource(time.Now().UnixNano()))

func websocketpage() {
	// flag.Parse()
	log.Println("START 2")
	hubs = map[string]*hub{
		"time": &hub{connections: make(map[*websocket.Conn]bool)},
	}
	go func() {
		for {
			for ws := range hubs["time"].connections {
				accel := <-accelerometr
				tmp := strings.Split(accel, ";")

				if len(tmp) == 4 {
					x := time.Now().UnixNano() / 1000000
					valX, _ := strconv.ParseFloat(tmp[0], 64)
					valY, _ := strconv.ParseFloat(tmp[1], 64)
					valZ, _ := strconv.ParseFloat(tmp[2], 64)
					event := &Event{TimeStamp: x, FloatXValue: valX, FloatYValue: valY, FloatZValue: valZ}
					websocket.JSON.Send(ws, *event)
					//log.Println(x, valX, valY, valZ)
				}
			}
		}
	}()

	http.Handle("/ws", websocket.Handler(wsServer))
	http.Handle("/", http.FileServer(http.Dir("static")))
	if openBrowser {
		openUrlInBrowser("http://localhost:" + strconv.Itoa(port) + "/index.html")
	}
	err := http.ListenAndServe(fmt.Sprintf(":%d", port), nil)
	if err != nil {
		panic("ListenANdServe: " + err.Error())
	}
}

func openUrlInBrowser(url string) {
	var err error
	switch runtime.GOOS {
	case "linux":
		err = exec.Command("xdg-open", url).Start()
	case "windows":
		err = exec.Command(`C:\Windows\System32\rundll32.exe`, "url.dll,FileProtocolHandler", url).Start()
	case "darwin":
		err = exec.Command("open", url).Start()
	default:
		log.Print("unsupported platform")
	}
	if err != nil {
		panic(err)
	}
}
